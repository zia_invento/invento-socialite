<?php

use Illuminate\Support\Facades\Route;
use Invento\Socialite\Controllers\SocialLoginController;

Route::group(['middleware'=>['check.banned.ip','throttle:40,1','web', 'auth','permission'], 'prefix' => 'admin', 'as' => 'admin.'], function (){

    Route::get('packages/config-socialite', [SocialLoginController::class, 'config'])->name('packages.socialite-configuration');
    Route::post('packages/socialite-store', [SocialLoginController::class, 'store'])->name('packages.store-socialite-configuration');

});

Route::name('login.socialite.')->prefix('login.socialite')->group(function () {
    Route::get('google', [SocialLoginController::class, 'redirectToGoogle'])->name('google');
    Route::get('google/callback', [SocialLoginController::class, 'handleGoogleCallback']);

    Route::get('facebook', [SocialLoginController::class, 'redirectToFacebook'])->name('facebook');
    Route::get('facebook/callback', [SocialLoginController::class, 'handleFacebookCallback']);

    Route::get('linkedin', [SocialLoginController::class, 'redirectToLinkedIn'])->name('linkedin');
    Route::get('linkedin/callback', [SocialLoginController::class, 'handleLinkedInCallback']);

    Route::get('github', [SocialLoginController::class, 'redirectToGitHub'])->name('github');
    Route::get('github/callback', [SocialLoginController::class, 'handleGitHubCallback']);
});

