<?php

return [
    'socialite_configuration' => 'Socialite Configuration',
    'client_id' => 'Client ID',
    'client_secret' => 'Client Secret',
    'redirect_url' => 'Redirect URL',
    'status' => 'Status',

    'facebook_login' => 'Facebook Login',
    'google_login' => 'Google Login',
    'github_login' => 'Github Login',
    'linkedin_login' => 'LinkedIn Login',
    'submit' => 'Submit',
];