<x-default-layout>

    @section('title')
        {{ __('socialite::socialite.socialite_configuration') }}
    @endsection

    @section('breadcrumbs')
        {{ Breadcrumbs::render('socialite.config') }}
    @endsection

    <div class="card card-flush">

        <div class="card-body pt-10" id="analytics_config">
            <div class="row">
                <div class="col-md-6 mb-10">
                    <div class="card card-flush" data-select2-id="select2-data-131-v40c">
                        <div class="card-header">
                            <div class="card-title">
                                <h2>{{ __('socialite::socialite.facebook_login') }}</h2>
                            </div>
                        </div>


                        <div class="card-body pt-0" data-select2-id="select2-data-130-pn13">
                            <form action="{{ route('admin.packages.store-socialite-configuration') }}" method="post">
                                @csrf

                                <input type="hidden" value="facebook" name="socialite">

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_id') }}</label>
                                    <input type="text" name="client_id" id="client_id" class="form-control mb-2"
                                           value="{{ isset($facebook['client_id']) ? $facebook['client_id'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_secret') }}</label>
                                    <input type="text" name="client_secret" id="client_secret" class="form-control mb-2"
                                           value="{{ isset($facebook['client_secret']) ? $facebook['client_secret'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.redirect_url') }}</label>
                                    <input type="text" name="redirect_url" id="redirect_url" class="form-control mb-2"
                                           value="{{ isset($facebook['redirect_url']) ? $facebook['redirect_url'] : '' }}"/>

                                </div>

                                <div class="mb-10">
                                    <label class="form-check form-switch form-check-custom form-check-solid">
                                        <input class="form-check-input" name="status" type="checkbox" {{ (isset($facebook['status']) && $facebook['status']) ? 'checked' : '' }} />
                                        <span class="form-check-label fw-semibold text-muted">{{ __('socialite::socialite.status') }}</span>
                                    </label>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-info btn-sm"><span
                                                class="indicator-label">{{  __('socialite::socialite.submit') }}</span></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 mb-10">
                    <div class="card card-flush" data-select2-id="select2-data-131-v40c">
                        <div class="card-header">
                            <div class="card-title">
                                <h2>{{ __('socialite::socialite.google_login') }}</h2>
                            </div>
                        </div>

                        <div class="card-body pt-0" data-select2-id="select2-data-130-pn13">
                            <form action="{{ route('admin.packages.store-socialite-configuration') }}" method="post">
                                @csrf

                                <input type="hidden" value="google" name="socialite">

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_id') }}</label>
                                    <input type="text" name="client_id" id="client_id" class="form-control mb-2"
                                           value="{{ isset($google['client_id']) ? $google['client_id'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_secret') }}</label>
                                    <input type="text" name="client_secret" id="client_secret" class="form-control mb-2"
                                           value="{{ isset($google['client_secret']) ? $google['client_secret'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.redirect_url') }}</label>
                                    <input type="text" name="redirect_url" id="redirect_url" class="form-control mb-2"
                                           value="{{ isset($google['redirect_url']) ? $google['redirect_url'] : '' }}"/>

                                </div>

                                <div class="mb-10">
                                    <label class="form-check form-switch form-check-custom form-check-solid">
                                        <input class="form-check-input" name="status" type="checkbox" {{ (isset($google['status']) && $google['status']) ? 'checked' : '' }} />
                                        <span class="form-check-label fw-semibold text-muted">{{ __('socialite::socialite.status') }}</span>
                                    </label>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-info btn-sm"><span
                                                class="indicator-label">{{  __('socialite::socialite.submit') }}</span></button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 mb-10">
                    <div class="card card-flush" data-select2-id="select2-data-131-v40c">
                        <div class="card-header">
                            <div class="card-title">
                                <h2>{{ __('socialite::socialite.github_login') }}</h2>
                            </div>
                        </div>

                        <div class="card-body pt-0" data-select2-id="select2-data-130-pn13">
                            <form action="{{ route('admin.packages.store-socialite-configuration') }}" method="post">
                                @csrf

                                <input type="hidden" value="github" name="socialite">

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_id') }}</label>
                                    <input type="text" name="client_id" id="client_id" class="form-control mb-2"
                                           value="{{ isset($github['client_id']) ? $github['client_id'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_secret') }}</label>
                                    <input type="text" name="client_secret" id="client_secret" class="form-control mb-2"
                                           value="{{ isset($github['client_secret']) ? $github['client_secret'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.redirect_url') }}</label>
                                    <input type="text" name="redirect_url" id="redirect_url" class="form-control mb-2"
                                           value="{{ isset($github['redirect_url']) ? $github['redirect_url'] : '' }}"/>

                                </div>

                                <div class="mb-10">
                                    <label class="form-check form-switch form-check-custom form-check-solid">
                                        <input class="form-check-input" name="status" type="checkbox" {{ (isset($github['status']) && $github['status']) ? 'checked' : '' }} />
                                        <span class="form-check-label fw-semibold text-muted">{{ __('socialite::socialite.status') }}</span>
                                    </label>
                                </div>


                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-info btn-sm"><span
                                                class="indicator-label">{{  __('socialite::socialite.submit') }}</span></button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 mb-10">
                    <div class="card card-flush" data-select2-id="select2-data-131-v40c">
                        <div class="card-header">
                            <div class="card-title">
                                <h2>{{ __('socialite::socialite.linkedin_login') }}</h2>
                            </div>
                        </div>

                        <div class="card-body pt-0" data-select2-id="select2-data-130-pn13">
                            <form action="{{ route('admin.packages.store-socialite-configuration') }}" method="post">
                                @csrf

                                <input type="hidden" value="linkedin" name="socialite">

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_id') }}</label>
                                    <input type="text" name="client_id" id="client_id" class="form-control mb-2"
                                           value="{{ isset($linkedin['client_id']) ? $linkedin['client_id'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.client_secret') }}</label>
                                    <input type="text" name="client_secret" id="client_secret" class="form-control mb-2"
                                           value="{{ isset($linkedin['client_secret']) ? $linkedin['client_secret'] : '' }}"/>

                                </div>

                                <div class="mb-10 fv-row">
                                    <label
                                            class="form-label">{{ __('socialite::socialite.redirect_url') }}</label>
                                    <input type="text" name="redirect_url" id="redirect_url" class="form-control mb-2"
                                           value="{{ isset($linkedin['redirect_url']) ? $linkedin['redirect_url'] : '' }}"/>

                                </div>

                                <div class="mb-10">
                                    <label class="form-check form-switch form-check-custom form-check-solid">
                                        <input class="form-check-input" name="status" type="checkbox" {{ (isset($linkedin['status']) && $linkedin['status']) ? 'checked' : '' }} />
                                        <span class="form-check-label fw-semibold text-muted">{{ __('socialite::socialite.status') }}</span>
                                    </label>
                                </div>


                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-info btn-sm"><span
                                                class="indicator-label">{{  __('socialite::socialite.submit') }}</span></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


</x-default-layout>

