<?php

namespace Invento\Socialite\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Brian2694\Toastr\Facades\Toastr;
use Spatie\Valuestore\Valuestore;

class SocialLoginController extends Controller
{

    public function config(){
        $this->store = Valuestore::make(resource_path('settings/settings.json'));

        if($this->store->has('socialite')){
            isset($this->store->get('socialite')['facebook']) ? $data['facebook'] = $this->store->get('socialite')['facebook'] : $data['facebook'] = [];
            isset($this->store->get('socialite')['google']) ? $data['google'] = $this->store->get('socialite')['google'] : $data['google'] = [];
            isset($this->store->get('socialite')['github']) ? $data['github'] = $this->store->get('socialite')['github'] : $data['github'] = [];
            isset($this->store->get('socialite')['linkedin']) ? $data['linkedin'] = $this->store->get('socialite')['linkedin'] : $data['linkedin'] = [];
        }else{
            $data[] = [
                "facebook" => [],
                "google" => [],
                "github" => [],
                "linkedin" => []
            ];
        }

        return view('socialite::config',$data);
    }

    public function store(Request $request){
        $this->store = Valuestore::make(resource_path('settings/settings.json'));

        $socialite['client_id'] = $request->client_id;
        $socialite['client_secret'] = $request->client_secret;
        $socialite['redirect_url'] = $request->redirect_url;
        $socialite['status'] = $request->has('status');

        $storeData = $this->store->get('socialite');

        if($request->socialite == 'facebook'){
            $storeData['facebook'] = $socialite;
        }elseif($request->socialite == 'google'){
            $storeData['google'] = $socialite;
        }elseif($request->socialite == 'github'){
            $storeData['github'] = $socialite;
        }elseif($request->socialite == 'linkedin'){
            $storeData['linkedin'] = $socialite;
        }

        $data['socialite'] = $storeData;

        $this->store->put($data);

        Toastr::success('Successfully Updated',"Socialite");

        return back();
    }
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        return $this->_handleCallback('google');
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        return $this->_handleCallback('facebook');
    }

    public function redirectToLinkedIn()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function handleLinkedInCallback()
    {
        return $this->_handleCallback('linkedin');
    }

    public function redirectToGitHub()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleGitHubCallback()
    {
        return $this->_handleCallback('github');
    }

    protected function _handleCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
            $authUser = $this->_registerOrLoginUser($user, $provider);
            Auth::login($authUser, true);
            return redirect()->route('home');
        } catch (Exception $e) {
            return redirect()->route('login');
        }
    }

    protected function _registerOrLoginUser($data, $provider)
    {
        $user = User::where('email', $data->email)->first();

        if (!$user) {
            $user = new User();
            $user->name = $data->name;
            $user->email = $data->email;
            $user->password = Hash::make($data->id);
            $user->save();
        }

        return $user;
    }
}