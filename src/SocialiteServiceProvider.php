<?php

namespace Invento\Socialite;

use Illuminate\Support\ServiceProvider;

class SocialiteServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/socialite.php', 'socialite');
    }

    public function boot()
    {
        // Publish configuration
        $this->publishes([
            __DIR__.'/../config/socialite.php' => config_path('socialite.php'),
        ], 'socialite-config');

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'socialite');

        // Load views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'socialite');

        // Publish views
        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/socialite'),
        ], 'socialite-views');


        //lang
        $this->publishes([
            __DIR__.'/../resources/lang' => $this->app->langPath('vendor/socialite')
        ],'socialite-lang');


        // Load routes
      //  $this->loadRoutesFrom(__DIR__.'/../routes/breadcrumbs.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }
}